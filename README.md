# README #

## What is this repository for? ##

* Application for uploading and viewing transactions

### How do I get set up? ###

* Configuration
   - Install Angular packages under the `WebUI` > `ClientApp`
* Database configuration
   - Run `script.sql` inside the project `Files` folder to create Sql database
* To locally run the app with Visual Studio
   - Open the solution with Visual Studio
   - **For API:** run `TransactionUploader.API` project in Visual Studio
   - **For Web UI:** run `WebUI` project in Visual Studio