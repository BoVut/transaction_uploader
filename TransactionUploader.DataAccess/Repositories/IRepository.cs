﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using TransactionUploader.Domain.Entities;

namespace TransactionUploader.DataAccess.Repositories
{
    public interface IRepository<T> where T : class
    {
        /// <summary>
        /// Insert entities
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public Task<int> Create(IEnumerable<T> entities);

        /// <summary>
        /// Get entities with predicate
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public Task<List<T>> GetAllAsync(Expression<Func<T, bool>> predicate);
    }
}
