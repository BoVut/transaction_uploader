﻿using System;
using System.Collections.Generic;
using System.Text;
using TransactionUploader.Domain.Entities;

namespace TransactionUploader.DataAccess.Repositories
{
    public interface ITransactionRepository : IRepository<Transaction>
    {
    }
}
