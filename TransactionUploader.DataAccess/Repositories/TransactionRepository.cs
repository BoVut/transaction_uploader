﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using TransactionUploader.Domain.Entities;

namespace TransactionUploader.DataAccess.Repositories
{
    public class TransactionRepository : ITransactionRepository
    {
        private readonly TestDbContext _context;

        public TransactionRepository(TestDbContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public async Task<int> Create(IEnumerable<Transaction> data)
        {
            _context.Transactions.AddRange(data);
            return await _context.SaveChangesAsync();
        }

        public async Task<List<Transaction>> GetAllAsync(Expression<Func<Transaction, bool>> predicate)
        {
            return await _context.Transactions.Where(predicate).ToListAsync();
        }
    }
}
