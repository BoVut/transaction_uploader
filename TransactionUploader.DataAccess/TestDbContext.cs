﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TransactionUploader.Domain.Entities;

namespace TransactionUploader.DataAccess
{
    public class TestDbContext : DbContext
    {
        public TestDbContext(DbContextOptions<TestDbContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Transaction>().Property(x => x.Amount).HasPrecision(18, 4);
        }

        public DbSet<Transaction> Transactions { get; set; }

        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            foreach (var entityEntry in ChangeTracker.Entries()) 
            {
                if (entityEntry.Entity is BaseEntity entity)
                {
                    // Add time stamp
                    var now = DateTime.UtcNow;
                    if(entity.CreatedDate == DateTime.MinValue)
                    {
                        entity.CreatedDate = now;
                    }
                    entity.LastModifiedDate = now;
                }
            }
            return await base.SaveChangesAsync();
        }
    }
}
