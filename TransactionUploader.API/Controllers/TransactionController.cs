﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TransactionUploader.Domain.Constants;
using TransactionUploader.Domain.Models;
using TransactionUploader.Service.Services;

namespace TransactionUploader.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TransactionController : ControllerBase
    {
        private readonly IFileUploadService _fileUploadService;
        private readonly ITransactionService _transactionService;

        public TransactionController(
            IFileUploadService fileUploadService,
            ITransactionService transactionService)
        {
            _fileUploadService = fileUploadService 
                ?? throw new ArgumentNullException(nameof(fileUploadService));
            _transactionService = transactionService 
                ?? throw new ArgumentNullException(nameof(transactionService));
        }

        [HttpGet]
        public async Task<ResultModel<List<TransactionOutDto>>> GetAsync([FromQuery]TransactionFilterModel filter)
        {
            try
            {
                return await _transactionService.GetAllAsync(filter);
            }
            catch (Exception ex)
            {
                var result = new ResultModel<List<TransactionOutDto>>
                {
                    Message = ex.Message
                };
                return result;
            }
        }

        [HttpPost]
        public async Task<IActionResult> PostAsync(IFormFile file)
        {
            if (file == null || file.Length == 0) 
                return BadRequest("Upload file is empty.");

            ResultModel<int> result;
            try
            {
                switch (file.ContentType?.Trim().ToLower())
                {
                    case FileContentType.TEXT_CSV:
                    case FileContentType.VDN_EXCEL:
                        result = await _fileUploadService.UploadTransactionFromCsvFileAsync(file.OpenReadStream());
                        break;
                    case FileContentType.TEXT_XML:
                    case FileContentType.APPLICATION_XML:
                        result = await _fileUploadService.UploadTransactionFromXmlFileAsync(file.OpenReadStream());
                        break;
                    default:
                        return BadRequest("Unknown file format.");
                }

                if (result.IsSuccess)
                    return Ok();
                else
                    return BadRequest(result.Message);
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }
    }
}
