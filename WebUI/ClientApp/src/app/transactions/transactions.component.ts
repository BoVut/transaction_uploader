import { formatDate } from '@angular/common';
import { Component, Inject } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Transaction } from '../models/transaction';

@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.scss']
})
export class TransactionsComponent {
  private readonly transactionEndpoint: string;

  private statusOptions = [
    { text:"Approved", value: "A"},
    { text:"Rejected", value: "R"},
    { text:"Done", value: "D"}
  ];

  public transactions: Transaction[];
  form = new FormGroup({
    fromDate: new FormControl(''),
    status: new FormControl('', [Validators.maxLength(1), Validators.minLength(1)]),
    currencyCode: new FormControl('', [Validators.maxLength(3), Validators.minLength(3)]),
  });

  get fromDate(): Date {
    return this.form.get('fromDate').value;
  }

  get status(): any {
    return this.form.get('status').value;
  }

  get currencyCode(): any {
    return this.form.get('currencyCode').value;
  }

  constructor(private readonly http: HttpClient, @Inject('API_URL') baseUrl: string) {
    this.transactionEndpoint = baseUrl + 'transaction';
    this.loadTransactions();
  }

  public loadTransactions() {
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json; charset=utf-8');

    let url = this.transactionEndpoint;
    url = this.addQueryString(url);

    this.http.get<Transaction[]>(url, { headers: headers })
      .subscribe((result: any) => {
        if (result.isSuccess) {
          const transactions = result.data.map((data: any) => new Transaction(data));
          this.transactions = transactions;
        }
      }, error => console.error(error));
  }

  private addQueryString(url: string) {
    const filters = new Array<string>();

    if (this.fromDate) {
      const dateString = formatDate(this.fromDate, 'dd-MMM-yyyy', 'en-US');
      filters.push("fromDate=" + dateString);
    }

    if (this.status) {
      filters.push("status=" + this.status);
    }

    if (this.currencyCode) {
      filters.push("currencyCode=" + this.currencyCode);
    }

    if (filters.length > 0) {
      const queryString = `?${filters.join('&')}`;
      url += queryString;
    }

    return url;
  }
}
