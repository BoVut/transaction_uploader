import { HttpClient } from '@angular/common/http';
import { Component, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { Transaction } from '../models/transaction';

@Component({
  selector: 'app-file-upload-component',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.scss']
})
export class FileUploadComponent {
  private readonly transactionEndpoint: string;

  public targetFile: File;
  public baseUrl: string;
  public uploadResult: string;
  public uploadSuccess: boolean;

  constructor(
    private readonly http: HttpClient, 
    @Inject('API_URL') baseUrl: string) {
    this.transactionEndpoint = baseUrl + 'transaction';
  }

  public upload() {
    if (!this.targetFile)
      return;

    let formData: FormData = new FormData();
    formData.append('file', this.targetFile, this.targetFile.name);

    this.http.post<Transaction[]>(this.transactionEndpoint, formData)
      .subscribe(() => {
        this.uploadSuccess = true;
        this.uploadResult = "Transaction upload completed.";
      }, error => {
        console.log(error);
        this.uploadSuccess = false;
        this.uploadResult = error.error;
      });
  }

  fileChange(event: { target: { files: FileList; }; }) {
    this.uploadResult = null;
    let fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      this.targetFile = fileList[0];
    }
  }
}
