export class Transaction {
    id: string;
    amount: number;
    currencyCode: string;
    status: string;

    constructor(data: any) {
        if (data) {
            this.id = data.id;
            this.amount = data.amount;
            this.currencyCode = data.currencyCode;
            this.status = data.status;
        }
    }

    get payment(): string {
        return `${this.amount.toString()} ${this.currencyCode}`;
    }
}