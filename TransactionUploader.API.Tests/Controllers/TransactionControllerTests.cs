﻿using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TransactionUploader.API.Controllers;
using TransactionUploader.Domain.Models;
using TransactionUploader.Service.Services;
using Xunit;

namespace TransactionUploader.API.Tests.Controllers
{
    public class TransactionControllerTests
    {
        private readonly Mock<IFileUploadService> _fileUploadServiceMock;
        private readonly Mock<ITransactionService> _transactionServiceMock;

        private readonly TransactionController _target;

        public TransactionControllerTests()
        {
            _fileUploadServiceMock = new Mock<IFileUploadService>();
            _transactionServiceMock = new Mock<ITransactionService>();

            _target = new TransactionController(
                _fileUploadServiceMock.Object, 
                _transactionServiceMock.Object);
        }

        [Fact]
        public async Task GetAsync_HasResults_ReturnExpectedRusults()
        {
            // Arrange
            var outDtos = new List<TransactionOutDto>
            {
                new TransactionOutDto { Id = "id 1" },
                new TransactionOutDto { Id = "id 2" }
            };

            var resultModel = new ResultModel<List<TransactionOutDto>>
            {
                Data = outDtos,
                IsSuccess = true
            };

            _transactionServiceMock.Setup(x => x.GetAllAsync(It.IsAny<TransactionFilterModel>()))
                .ReturnsAsync(resultModel);

            // Act
            var result = await _target.GetAsync(new TransactionFilterModel());

            // Assert
            Assert.True(result.IsSuccess);
            Assert.NotNull(result.Data);
            Assert.Equal(outDtos.Count, result.Data.Count);
            _transactionServiceMock.Verify(x => x.GetAllAsync(It.IsAny<TransactionFilterModel>()), Times.Once);
        }
    }
}
