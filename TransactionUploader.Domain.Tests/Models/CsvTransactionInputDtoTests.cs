﻿using System;
using System.Collections.Generic;
using System.Text;
using TransactionUploader.Domain.Models;
using Xunit;

namespace TransactionUploader.Domain.Tests.Models
{
    public class CsvTransactionInputDtoTests
    {
        private const string DefaultAmount = "100.789";
        private const string DefaultCode = "THB";
        private const string DefaultDate = "16/01/2021 21:11:11";
        private const string DefaultStatus = "Approved";

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData("  ")]
        public void Validate_EmptyIdValue_ReturnsErrorResultModel(string id)
        {
            // Arrange 
            var dto = new CsvTransactionInputDto(
                id, 
                DefaultAmount, 
                DefaultCode, 
                DefaultDate, 
                DefaultStatus);

            // Act
            var result = dto.Validate();

            // Assert
            Assert.False(result.IsSuccess);
            Assert.Equal("Id is empty.", result.Message);
        }

        //TODO: Add more test cases
    }
}
