﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TransactionUploader.Domain.Constants
{
    public static class TransactionStatus
    {
        public const string APPROVED = "A";
        public const string REJECTED = "R";
        public const string DONE = "D";
    }
}
