﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TransactionUploader.Domain.Constants
{
    public class FileContentType
    {
        public const string TEXT_CSV = "text/csv";
        public const string VDN_EXCEL = "application/vnd.ms-excel";
        public const string APPLICATION_XML = "application/xml";
        public const string TEXT_XML = "text/xml";
    }
}
