﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace TransactionUploader.Domain.Constants
{
    public class Regions
    {
        private static List<string> _currencyCodes { get; set; }

        public static List<string> AllIsoCurrencySymbols
        {
            get
            {
                if (_currencyCodes == null || !_currencyCodes.Any())
                {
                    var cultures = CultureInfo.GetCultures(CultureTypes.SpecificCultures);
                    _currencyCodes = cultures.Select(x => new RegionInfo(x.Name).ISOCurrencySymbol)
                        .Distinct().ToList();
                }
                return _currencyCodes;
            }
        }
    }
}
