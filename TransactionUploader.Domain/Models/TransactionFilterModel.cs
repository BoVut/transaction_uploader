﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TransactionUploader.Domain.Models
{
    public class TransactionFilterModel
    {
        public string CurrencyCode { get; set; }
        public string Status { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
    }
}
