﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Xml.Serialization;
using TransactionUploader.Domain.Constants;

namespace TransactionUploader.Domain.Models
{
    public class XmlTransactionInputDto
    {
        private readonly string[] AllowedStatuses = new string[] { 
            STATUS_APPROVED, 
            STATUS_REJECTED, 
            STATUS_DONE 
        };
        private readonly string[] ExpectedDateTimeFormat = new string[] { 
            "yyyy-MM-ddTHH:mm:ss", 
            "yyyy-MM-ddThh:mm:ss" 
        };
        private const int MaxIdLength = 50;

        public const string STATUS_APPROVED = "approved";
        public const string STATUS_REJECTED = "rejected";
        public const string STATUS_DONE = "done";

        [XmlAttribute("id")]
        public string Id { get; set; }
        public string TransactionDate { get; set; }
        public PaymentDetailsInputDto PaymentDetails { get; set; }
        public string Status { get; set; }

        public ResultModel<string> Validate()
        {
            var errors = new List<string>();
            // Id
            if (string.IsNullOrWhiteSpace(Id))
                errors.Add("Id is empty.");
            else if (Id.Length > MaxIdLength)
                errors.Add("Transaction Id is longer than 50 characters.");

            // Payment
            if (PaymentDetails == null)
            {
                errors.Add("Missing payment details.");
            }
            else
            {
                // Amount
                string amount = PaymentDetails.Amount;
                if (string.IsNullOrWhiteSpace(amount))
                    errors.Add("Amount is empty.");
                else if (!decimal.TryParse(amount, out _))
                    errors.Add($"Invalid amount value '{amount}'.");

                // Currency Code
                string currencyCode = PaymentDetails.CurrencyCode;
                if (string.IsNullOrWhiteSpace(currencyCode))
                {
                    errors.Add("Currency code is empty.");
                }
                else
                {
                    bool currencyExists = Regions.AllIsoCurrencySymbols.
                        Any(x => x.Equals(currencyCode, StringComparison.InvariantCultureIgnoreCase));

                    if (!currencyExists)
                        errors.Add($"Invalid currency code '{currencyCode}'.");
                }
            }

            // Transaction Date
            if (string.IsNullOrWhiteSpace(TransactionDate))
            {
                errors.Add("Transaction date is empty.");
            }
            else
            {
                bool canParse = DateTime.TryParseExact(
                    TransactionDate, 
                    ExpectedDateTimeFormat, 
                    CultureInfo.InvariantCulture, 
                    DateTimeStyles.None, out _);

                if (!canParse)
                    errors.Add($"Invalid Transaction date: '{TransactionDate}'.");
            }

            // Status
            if (string.IsNullOrWhiteSpace(Status))
                errors.Add("Status is empty.");
            else if (!AllowedStatuses.Contains(Status.Trim().ToLower()))
                errors.Add($"Invalid status `{Status}`");

            var result = new ResultModel<string>();
            if (errors.Count > 0)
            {
                result.Message = string.Join(',', errors);
            }
            else
            {
                result.IsSuccess = true;
                result.Message = string.Empty;
            }

            return result;
        }
    }

    public class PaymentDetailsInputDto
    {
        public string Amount { get; set; }
        public string CurrencyCode { get; set; }
    }
}
