﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using TransactionUploader.Domain.Constants;

namespace TransactionUploader.Domain.Models
{
    public class CsvTransactionInputDto
    {
        private readonly string[] AllowedStatuses = new string[] { 
            STATUS_APPROVED, 
            STATUS_FAILED, 
            STATUS_FINISHED 
        };
        private readonly string[] AllowedDateTimeFormats = new string[] { 
            "dd/MM/yyyy hh:mm:ss", 
            "dd/MM/yyyy HH:mm:ss" 
        };
        private const int MaxIdLength = 50;

        public const string STATUS_APPROVED = "approved";
        public const string STATUS_FAILED = "failed";
        public const string STATUS_FINISHED = "finished";

        public string Id { get; }
        public string Amount { get; }
        public string CurrencyCode { get; }
        public string TransactionDate { get; }
        public string Status { get; }

        public CsvTransactionInputDto(string id, string amount, string code, string date, string status)
        {
            Id = id;
            Amount = amount;
            CurrencyCode = code;
            TransactionDate = date;
            Status = status;
        }

        public ResultModel<string> Validate()
        {
            var errors = new List<string>();

            // Id
            if (string.IsNullOrWhiteSpace(Id))
                errors.Add("Id is empty.");
            else if (Id.Length > MaxIdLength)
                errors.Add("Transaction Id is longer than 50 characters.");

            // Amount
            if (string.IsNullOrWhiteSpace(Amount))
                errors.Add("Amount is empty.");
            else if (!decimal.TryParse(Amount, out _))
                errors.Add($"Invalid amount value '{Amount}'.");

            // Currency Code
            if (string.IsNullOrWhiteSpace(CurrencyCode))
            {
                errors.Add("Currency code is empty.");
            }
            else
            {
                bool currencyExists = Regions.AllIsoCurrencySymbols.
                    Any(x => x.Equals(CurrencyCode, StringComparison.InvariantCultureIgnoreCase));
                if (!currencyExists)
                    errors.Add($"Invalid currency code '{CurrencyCode}'.");
            }

            // Transaction Date
            if (string.IsNullOrWhiteSpace(TransactionDate))
            {
                errors.Add("Transaction date is empty.");
            }
            else
            {
                bool canParse = DateTime.TryParseExact(
                    TransactionDate,
                    AllowedDateTimeFormats,
                    CultureInfo.InvariantCulture,
                    DateTimeStyles.None, out _);

                if (!canParse)
                    errors.Add($"Invalid Transaction date: '{TransactionDate}'.");
            }

            // Status
            if (string.IsNullOrWhiteSpace(Status))
                errors.Add("Status is empty.");
            else if (!AllowedStatuses.Contains(Status.Trim().ToLower()))
                errors.Add($"Invalid status `{Status}`");

            // Result
            var result = new ResultModel<string>();
            if (errors.Count > 0)
            {
                result.Message = string.Join(',', errors);
            }
            else
            {
                result.IsSuccess = true;
                result.Message = string.Empty;
            }

            return result;
        }
    }
}
