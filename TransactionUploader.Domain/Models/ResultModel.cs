﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TransactionUploader.Domain.Models
{
    public class ResultModel<T>
    {
        public bool IsSuccess { get; set; } = false;
        public string Message { get; set; } = "Unknown Error";
        public T Data { get; set; }
    }
}
