﻿using AutoMapper;
using System;
using TransactionUploader.Domain.Constants;
using TransactionUploader.Domain.Entities;
using TransactionUploader.Domain.Models;

namespace TransactionUploader.Domain.Mappers
{
    public class TransactionProfile : Profile
    {
        public TransactionProfile()
        {
            CreateMap<Transaction, TransactionOutDto>();
            CreateMap<CsvTransactionInputDto, Transaction>()
                .ForMember(dest => dest.Status, opt => opt.MapFrom((src, dest) =>
                {
                    return src.Status.Trim().ToLower() switch
                    {
                        CsvTransactionInputDto.STATUS_APPROVED => TransactionStatus.APPROVED,
                        CsvTransactionInputDto.STATUS_FAILED => TransactionStatus.REJECTED,
                        CsvTransactionInputDto.STATUS_FINISHED => TransactionStatus.DONE,
                        _ => throw new ArgumentException($"Unknown status {src.Status}"),
                    };
                }));
            CreateMap<XmlTransactionInputDto, Transaction>()
                .ForMember(dest => dest.Amount, opt => opt.MapFrom(src => src.PaymentDetails.Amount))
                .ForMember(dest => dest.CurrencyCode, opt => opt.MapFrom(src => src.PaymentDetails.CurrencyCode))
                .ForMember(dest => dest.Status, opt => opt.MapFrom((src, dest) =>
                {
                    return src.Status.Trim().ToLower() switch
                    {
                        XmlTransactionInputDto.STATUS_APPROVED => TransactionStatus.APPROVED,
                        XmlTransactionInputDto.STATUS_REJECTED => TransactionStatus.REJECTED,
                        XmlTransactionInputDto.STATUS_DONE => TransactionStatus.DONE,
                        _ => throw new ArgumentException($"Unknown status {src.Status}"),
                    };
                }));
        }

    }
}
