﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TransactionUploader.Domain.Entities
{
    public class Transaction : BaseEntity
    {
        public string Id { get; set; }
        public DateTime TransactionDate { get; set; }
        public decimal Amount { get; set; }
        public string CurrencyCode { get; set; }
        public string Status { get; set; }
    }
}
