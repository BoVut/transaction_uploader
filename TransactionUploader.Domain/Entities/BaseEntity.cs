﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TransactionUploader.Domain.Entities
{
    public class BaseEntity
    {
        public DateTime CreatedDate { get; set; }
        public DateTime LastModifiedDate { get; set; }
    }
}
