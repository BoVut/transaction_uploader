﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TransactionUploader.Domain.Models;

namespace TransactionUploader.Service.Services
{
    public interface ITransactionService
    {
        /// <summary>
        /// Get transactions
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public Task<ResultModel<List<TransactionOutDto>>> GetAllAsync(TransactionFilterModel filter);
    }
}
