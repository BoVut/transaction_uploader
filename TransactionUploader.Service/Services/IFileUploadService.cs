﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using TransactionUploader.Domain.Models;

namespace TransactionUploader.Service.Services
{
    public interface IFileUploadService
    {
        /// <summary>
        /// Upload transactions from CSV file stream
        /// </summary>
        /// <param name="fileStream"></param>
        /// <returns></returns>
        public Task<ResultModel<int>> UploadTransactionFromCsvFileAsync(Stream fileStream);

        /// <summary>
        /// Upload transactions from XML file stream
        /// </summary>
        /// <param name="fileStream"></param>
        /// <returns></returns>
        public Task<ResultModel<int>> UploadTransactionFromXmlFileAsync(Stream fileStream);
    }
}
