﻿using AutoMapper;
using LinqKit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using TransactionUploader.DataAccess.Repositories;
using TransactionUploader.Domain.Entities;
using TransactionUploader.Domain.Models;

namespace TransactionUploader.Service.Services
{
    public class TransactionService : ITransactionService
    {
        private readonly ITransactionRepository _transactionRepository;
        private readonly IMapper _mapper;

        public TransactionService(
            IMapper mapper,
            ITransactionRepository transactionRepository)
        {
            _mapper = mapper
                ?? throw new ArgumentNullException(nameof(mapper));
            _transactionRepository = transactionRepository
                ?? throw new ArgumentNullException(nameof(transactionRepository));
        }

        public async Task<ResultModel<List<TransactionOutDto>>> GetAllAsync(TransactionFilterModel filter = null)
        {
            var predicate = PredicateBuilder.New<Transaction>(true);

            predicate = GenerateFilterQuery(filter, predicate);

            List<Transaction> transactions = await _transactionRepository.GetAllAsync(predicate);

            var outDtos = _mapper.Map<List<Transaction>, List<TransactionOutDto>>(transactions);
            var resultModel = new ResultModel<List<TransactionOutDto>>
            {
                Data = outDtos,
                IsSuccess = true,
                Message = string.Empty
            };

            return resultModel;
        }

        private static Expression<Func<Transaction, bool>> GenerateFilterQuery(TransactionFilterModel filter, ExpressionStarter<Transaction> predicate)
        {
            if (!string.IsNullOrWhiteSpace(filter.CurrencyCode))
                predicate = predicate.And(x => x.CurrencyCode.ToLower() == filter.CurrencyCode.Trim().ToLower());
            if (!string.IsNullOrWhiteSpace(filter.Status))
                predicate = predicate.And(x => x.Status.ToLower() == filter.Status.Trim().ToLower());
            if (filter.FromDate.HasValue)
                predicate = predicate.And(x => x.TransactionDate.Date >= filter.FromDate.Value.Date);
            if (filter.ToDate.HasValue)
                predicate = predicate.And(x => x.TransactionDate.Date <= filter.ToDate.Value.Date);
            return predicate;
        }
    }
}
