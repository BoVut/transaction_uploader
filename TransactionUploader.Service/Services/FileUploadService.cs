﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using TransactionUploader.DataAccess.Repositories;
using TransactionUploader.Domain.Entities;
using TransactionUploader.Domain.Models;

namespace TransactionUploader.Service.Services
{
    public class FileUploadService : IFileUploadService
    {
        private readonly IMapper _mapper;
        private readonly ITransactionRepository _transactionRepository;
        private const int MaxFileSizeInKB = 1024;

        public FileUploadService(IMapper mapper, ITransactionRepository transactionRepository)
        {
            _mapper = mapper
                ?? throw new ArgumentNullException(nameof(mapper));
            _transactionRepository = transactionRepository
                ?? throw new ArgumentNullException(nameof(transactionRepository));
        }

        public async Task<ResultModel<int>> UploadTransactionFromCsvFileAsync(Stream fileStream)
        {
            var result = new ResultModel<int>();
            if(fileStream.Length > MaxFileSizeInKB)
            {
                result.Message = "Exceeded file size limit";
                return result;
            }

            var dtos = new List<CsvTransactionInputDto>();

            using TextFieldParser parser = new TextFieldParser(fileStream);
            parser.TextFieldType = FieldType.Delimited;
            parser.SetDelimiters(",");

            var sb = new StringBuilder();
            // TODO: check for header row
            while (!parser.EndOfData)
            {
                string[] cols = parser.ReadFields();
                if (cols.Length == 5)
                {
                    var dto = new CsvTransactionInputDto(id: cols[0], amount: cols[1], code: cols[2], date: cols[3], status: cols[4]);

                    ResultModel<string> propValidateResult = dto.Validate();

                    if (!propValidateResult.IsSuccess)
                        sb.AppendLine($"Invalid content \"{string.Join(',', cols)}\" with the following error: {propValidateResult.Message}");
                    else
                        dtos.Add(dto);
                }
                else
                {
                    sb.AppendLine($"Invalid row data: \"{string.Join(',', cols)}\"");
                }
            }

            if (sb.Length > 0)
            {
                result.Message = sb.ToString();
            }
            else
            {
                var transactions = _mapper.Map<IEnumerable<CsvTransactionInputDto>, IEnumerable<Transaction>>(dtos);
                await InsertTransactions(result, transactions);
            }

            return result;
        }


        public async Task<ResultModel<int>> UploadTransactionFromXmlFileAsync(Stream fileStream)
        {
            var result = new ResultModel<int>();
            if(fileStream.Length > MaxFileSizeInKB)
            {
                result.Message = "Exceeded file size limit";
                return result;
            }

            var dtos = new List<XmlTransactionInputDto>();

            var parser = new TextFieldParser(fileStream);
            string xmlFile = parser.ReadToEnd();
            var doc = new XmlDocument();
            doc.LoadXml(xmlFile);

            var sb = new StringBuilder();
            foreach (XmlNode transactionNode in doc.ChildNodes[0])
            {
                var serializer = new XmlSerializer(typeof(XmlTransactionInputDto), new XmlRootAttribute("Transaction"));
                string xmlContent = transactionNode.OuterXml;
                MemoryStream memStream = new MemoryStream(Encoding.UTF8.GetBytes(xmlContent));
                XmlReader reader = new XmlTextReader(memStream);
                if (serializer.CanDeserialize(reader))
                {
                    var dto = (XmlTransactionInputDto)serializer.Deserialize(reader);

                    ResultModel<string> propValidateResult = dto.Validate();

                    if (!propValidateResult.IsSuccess)
                        sb.AppendLine($"Invalid content \"{xmlContent}\" with the following error: {propValidateResult.Message}");
                    else
                        dtos.Add(dto);
                }
                else
                {
                    sb.AppendLine($"Invalid data format: \"{xmlContent}\"");
                }
            }

            if (sb.Length > 0)
            {
                result.Message = sb.ToString();
            }
            else
            {
                var transactions = _mapper.Map<IEnumerable<XmlTransactionInputDto>, IEnumerable<Transaction>>(dtos);
                await InsertTransactions(result, transactions);
            }

            return result;
        }

        private async Task InsertTransactions(ResultModel<int> result, IEnumerable<Transaction> transactions)
        {
            try
            {
                int createdCount = await _transactionRepository.Create(transactions);
                result.Data = createdCount;
                result.Message = string.Empty;
                result.IsSuccess = true;
            }
            catch (DbUpdateException dbException)
            {
                result.IsSuccess = false;
                result.Message = $"Failed to save data: {dbException.InnerException?.Message ?? dbException.Message}";
            }
        }
    }
}
